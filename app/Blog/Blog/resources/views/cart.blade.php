<!DOCTYPE html>
<html> 
    <head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
    <title>Checkout</title>
    <body>
        <table>
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
          
                <?php foreach (Cart::content() as $row) : ?>
                     {{ csrf_field() }}
                     <tr>
                             <td>
                                 <p><strong><?php echo $row->name; ?></strong></p>
                             <td>
                                 <form action="{{ route('cartUpdate', array('rowid' => $row->rowId)) }}" method="POST">
                                           <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                     <input type="text"  name="qty" value="<?php echo $row->qty; ?>">
                                     <input type="hidden" name="rowid" value="<?php echo $row->rowId; ?>"> 
                                     <div id="btnwrap">
                                         <button class="clsActionButton" id="idAddButton">Update</button>
                                 </form>
                             </td>

                        <td>$<?php echo $row->price; ?></td>
                        <td><?php echo Cart::subtotal(); ?></td>
                      <td><a href = 'deleteFromCart/{{ $row->rowId}}'>Delete</a></td></br>
                           </tr>
                <?php endforeach; ?>                           
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>Subtotal</td>
                <td><?php echo Cart::subtotal(); ?></td>
            </tr>
        </tfoot>
    </table>
    <a href='cart/checkout'><button>Checkout</button></a>
</body>

</html>
