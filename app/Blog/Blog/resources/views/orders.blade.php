<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>Orders</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
</head>
  <h1>Orders</h1>
                             
    <?php if (!empty($orders)): ?>
            
        <table class="admin_table">
            <tr>
                <th>Id</th>
                <th>From</th>
                <th>Address</th>
                <th>Amount</th>
                 <th>Details</th>
                 <th>Status</th>
                  <th>Order Action</th>
            </tr>

            <?php foreach ($orders as $key => $order): ?>
            <tr>
                <td><?php echo $order->order_Id; ?></td>
                <td><?php echo $order->username; ?></td>
                <td><?php echo $order->address; ?></td>
                <td><?php echo $order->amount; ?></td>
                </br>
                <td><a href="details/{{ $order->order_Id }}">Details</a></td>
                   <td>
                    <?php if ($order->status == 0): ?>
                        <?php echo 'Pending'; ?>
                    <?php elseif ($order->status == 1 ): ?>
                        <?php echo 'Completed'; ?>
                    <?php elseif ($order->status == 3 ): ?>
                        <?php echo 'Paid'; ?>
                    <?php else: ?>
                        <?php echo 'Canceled'; ?>
                    <?php endif; ?>
                </td>
                 <td>
                    <?php if ($order->status == 3): ?>
                        <a href="confirm/{{ $order->order_Id }}">Complete</a>
                    <?php endif; ?>
                
                    <?php if ($order->status != 1 && $order->status != 3): ?>
                        <a href="paid/{{ $order->order_Id }}">Set to Paid</a>
                    <?php endif; ?>
                
                    <?php if ($order->status != 2 && $order->status != 0): ?>
                        <a href="cancel/{{ $order->order_Id }}">Cancel</a>
                    <?php endif; ?>              
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <?php endforeach; ?>
            
            <?php else: ?>
            <p class="error">
                No results found.
            </p>
            <?php endif; ?>   
        </table>
</html>




