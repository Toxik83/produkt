<html>
   
   <head>
      <title>Edit Subcategory</title>
   </head> 
   <body>
        <h2>Edit Subcategory</h2>
         <div class="container">
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
       <form action="<?php echo URL::to('/');?>/SubCategoryController/<?=$subcategory->id?>" method="POST">
          {{ method_field('PUT') }}
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <label>Name :</label>
            <input type="text" name="sub_name" value ="<?php echo $subcategory->sub_name; ?>" ></br>
           <input type="hidden" name="_method" value="put">
            <input type="submit" value="Send" name="submit">
      </form>
   </body>
</html>