<!DOCTYPE html>
<html> 
    <title>Add Subcategory</title>
    <body>
        <h2>Add Subcategory</h2>
        <div class="container">
      <div class="container">
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
       <div class="messages">
        @if (Session::has('insertSub_message'))
        {{ Session::get('insertSub_message') }}
      @endif
       @if (Session::has('updateSub_message'))
        {{ Session::get('updateSub_message') }}
      @endif
        @if (Session::has('deleteSub_message'))
        {{ Session::get('deleteSub_message') }}
      @endif
      </div>
        <form action="createSub" method ="POST">
            {{ csrf_field() }}
            <label>Title :</label>
            <input type="text" name="sub_name" ></br>
            </br>
             <label>Category :</label>
            <select name ="category">
                <option value="Choose category">Choose category</option>
                <?php foreach ($categories as $category):?>
                <option  value ="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                <?php endforeach;?>
            </select>
            </br>
            <input type="submit" value="Save" name="submit">
            </br>
        </form>
        <h2>SubCategories</h2>   
        <?php if (!empty($subcategories)): ?>
            <?php foreach ($subcategories as $iKey => $aValue) : ?>
            <tr>
                <td><?php echo htmlspecialchars($aValue->name); ?></td></br>
                <td><a href = 'category/{{ $aValue->parent_id }}/sub/{{$aValue->id}}'>Add Product</a></td></br>
                <td><a href = 'SubCategoryController/{{ $aValue->id }}/edit'>Edit</a></td></br>
                <td><a href = 'deleteSub/{{ $aValue->id }}'>Delete</a></td></br>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <h4>There are no records yet</h4>
<?php endif; ?> 
</body>
</html>

