<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use App\models\Product;
use App\models\Subcategory;
use App\models\Category;
use App\models\Manufacturer;
class ProductController 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
     
    } 
  public function storeProduct(Request $request) {
           $v =  \Validator::make($request->all(), [
       'name' => 'required',
         'file' => 'required|mimes:jpeg,jpg,png',
           'subcategory' => 'required|integer',
         'price' => 'required|numeric',
    ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
    $cat_id=0;
   
        $name = $request->input('name');
         $sub_id = $request->input('subcategory');
         $manufacturer_id = $request->input('manufacturer');
         $price = $request->input('price');
         
         if(!$request->input('sizes') == null ){
             $sizes = $request->input('sizes');
         }
         else{
             $sizes = 0;
         }
         
         $oSubCategory = new Subcategory();
         $sub_cat = $oSubCategory->getSubCategoryById($sub_id);
         
         $oManufacturer = new Manufacturer();
         $manufacturer = $oManufacturer->getManufacturerById($manufacturer_id);
         
        foreach($sub_cat as $sub){
           $cat_id = $sub->parent_id;
        }
         $file_ext = explode('.', $_FILES['file']['name']);
        $ext = $file_ext[1];
        $allowed = array('jpg');

        $file_name = $_FILES['file']['name'];
         $oProduct = new Product();
         
         $id =  $oProduct->create($name,$cat_id, $price, $manufacturer[0]->id,$sub_id,$file_name,$sizes);
          $file_destination = 'C:/xampp/htdocs/Blog/Blog/storage/app/public/product/'.$id.'/';
         
          if(!file_exists($file_destination)){
              @mkdir($file_destination, 0777, true);

              if (in_array($ext, $allowed)) {
                  
            if (move_uploaded_file($_FILES['file']['tmp_name'], $file_destination.basename($_FILES['file']['name']))) {
                  \Session::flash('createProduct_message', 'The product has been created!');
           
                    $subCategories = $oSubCategory->getSubcategories();
                    $products = $oProduct->getProductsByCatIdAndSubId($cat_id, $sub_id);
                    return back();
                   // return view('addProduct', array('subcategories' => $subCategories, 'products' => $products));
                } else {
                echo "Upload failed";
            }
           }
           }
           
        $subCategories = $oSubCategory->getSubcategories();
        $products = $oProduct->getProductsByCatIdAndSubId($cat_id, $sub_id);
        
        return view('addProduct', array('subcategories' => $subCategories, 'products' => $products));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $oProduct = new Product();
      $product = $oProduct->getProductById($id);
      return \View::make("view")->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       if (Session::get('logged') == 1) {
       $oProduct = new Product();
     $product = $oProduct->getProductById($id);
     
     $oSubCategory = new Subcategory();
         $subcategories = $oSubCategory->getSubcategories();
         $oManufacturer = new Manufacturer();
         $manufacturers = $oManufacturer->getManufacturers();
     return view('product', array('subcategories' => $subcategories,'product' => $product, 'manufacturers'=>$manufacturers));
        } else {
           
        }
    }
    public function getPagedBlogs() {
        
    }

    public function update(Request $request, $id) {
               $v =  \Validator::make($request->all(), [
       'name' => 'required',
         'file' => 'required|mimes:jpeg,jpg,png',
           'subcategory' => 'required|integer',
         'price' => 'required|numeric',
    ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
     $cat_id=0;
        $name = $request->input('name');
        $sub_id = $request->input('subcategory');
        $manufacturer_id = $request->input('manufacturer');
         $price = $request->input('price');

        $oSubCategory = new Subcategory();
        $sub_cat = $oSubCategory->getSubCategoryById($sub_id);
        $oManufacturer = new Manufacturer();
        $manufacturer = $oManufacturer->getManufacturerById($manufacturer_id);
        foreach ($sub_cat as $sub) {
            $cat_id = $sub->parent_id;
        }
        
        $file_ext = explode('.', $_FILES['file']['name']);
        $ext = $file_ext[1];
        $allowed = array('jpg');

        $oProduct = new Product();
        $product = $oProduct->getProductById($id);
        
       // var_dump('C:/xampp/htdocs/Blog/Blog/storage/app/public/product/'.$id.'/'.$product[0]->image_name); die;
        unlink('C:/xampp/htdocs/Blog/Blog/storage/app/public/product/'.$id.'/'.$product[0]->image_name);

        $new_image = $_FILES['file']['name'];
         if (in_array($ext, $allowed)) {
            if (move_uploaded_file($_FILES['file']['tmp_name'], 'C:/xampp/htdocs/Blog/Blog/storage/app/public/product/'.$id.'/'.$new_image)) {
                //  echo "File is valid, and was successfully uploaded.\n";
            } else {
                echo "Upload failed";
            }
              $new_image = $_FILES['file']['name'];
               $oProduct->update($id, $name, $cat_id, $price,$manufacturer[0]->id, $sub_id, $new_image);
                  \Session::flash('updateProduct_message', 'The product has been updated!');
         }
        $subCategories = $oSubCategory->getSubcategories();
        $products = $oProduct->getProductsByCatIdAndSubId($cat_id, $sub_id);
        $manufacturers = $oManufacturer->getManufacturers();
        return view('addProduct', array('subcategories' => $subCategories,'products' => $products, 'manufacturers'=>$manufacturers));
    }

     public function getProducts($id=0,$sub_id=0, Request $request) {
        
        $oCategory = new Category();
        $categories= $oCategory::all();
         $oProduct = new Product();
         $oManufacturer = new Manufacturer();
         $manufacturers = $oManufacturer->getManufacturers();
         $oSubCategory = new Subcategory();
         
         if(!$request->input('sub')== null){
             $sub_id = $request->input('sub');
             $products = $oProduct->getPagedProducts($sub_id);
            return view('products', array('products'=> $products,'categories'=> $categories,'manufacturers'=> $manufacturers));
         }
         if(!$id== 0){
           
            $aSubs = $oSubCategory->getSubCategoriesByParentId($id); 
              return view('products', array('categories' => $categories,'manufacturers'=> $manufacturers, 'subcategories'=> $aSubs));
            
            
         }
         else{
            
               $products = $oProduct->getPagedProducts();
        return view('products', array('categories' => $categories,'products'=> $products, 'manufacturers'=> $manufacturers)); 
         }
         
         if(isset($_GET['man_id'])){
             
             $products = $oProduct->getPagedProducts(0,$_GET['man_id']);
            
            return view('products', array('categories' => $categories, 'products'=> $products,'manufacturers'=> $manufacturers ));
         }
         else{
           
            $products = $oProduct->getPagedProducts();
        return view('products', array('categories' => $categories,'products'=> $products, 'manufacturers'=> $manufacturers)); 
        }         
    }
      public function destroy($id) {
        
       $oProduct = new Product(); 
       $oProduct->deleteProduct($id);
        \Session::flash('deleteProduct_message', 'The product has been deleted!');
       return back();
        
    }
}
