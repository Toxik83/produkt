<?php

namespace App\Http\Controllers;
use App\models\Manufacturer;
use Illuminate\Http\Request;
use Session;

class ManufacturerController 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manufacturers() {
           $oManufacturer = new Manufacturer();
    $manufacturers =  $oManufacturer->getManufacturers();
   
    if(!empty($manufacturers)){
       
        return view('manufacturers', array('manufacturers' => $manufacturers));
    }
        return view('manufacturers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $v =  \Validator::make($request->all(), [
        'name' => 'required'
    ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
      $name = $request->input('name');
      $order_id = $request->input('order_id');
    
      $oManufacturer = new Manufacturer();
      $oManufacturer->create($name, $order_id);
       \Session::flash('insertManufacturer_message', 'The manufacturer has been created!');
       return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
    }
   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id) {
      $name = $request->input('name');
      $oManufacturer = new Manufacturer();
      $oManufacturer->update($id, $name);
       \Session::flash('updateMan_message', 'The manufacturer has been updated!');
      return redirect('/manufacturers');
    }

      public function subCategories()
    {
        return $this->belongsToMany('App\Role');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
       $oCategory = new Category();
       $oCategory->deleteCategory($id);
        \Session::flash('delete_message', 'The category has been deleted!');
        return redirect('/categories');
    }
       public function edit($id) {
        
         if (Session::get('logged') == 1) {
           
       $oManufacturer = new Manufacturer ();
     $manufacturer = $oManufacturer->getManufacturerById($id);
    
      return \View::make("manufacturer")->with("manufacturer", $manufacturer);
        } else {
           
        }
    }
  
}

