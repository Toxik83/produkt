<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\models\Category;
use App\models\Subcategory;
class CategoryController 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSubcategories() {

        $oCategory = new Category();
         $oSubcategory = new Subcategory();
        $categories = $oCategory->getCategories();
        if(empty($categories)){
            return redirect('/categories');
        }
        $subcategories = $oSubcategory->getSubcategories();
       
        return view('addSubCategory', array('categories' => $categories, 'subcategories' => $subcategories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
     $oCategory = new Category();
    $categories =  $oCategory->getCategories();
    if(!empty($categories)){
       
        return view('addCategory', array('categories' => $categories));
    }
      return view('addCategory');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSub(Request $request) {
        $v =  \Validator::make($request->all(), [
        'sub_name' => 'required',
         'category' => 'required|integer',
    ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
      $name = $request->input('sub_name');
      $parent_id = $request->input('category');
     
       $oSubcategory = new Subcategory;
       $oSubcategory->create($parent_id, $name);
       \Session::flash('insertSub_message', 'The subcategory has been created!');
       return redirect('/sub');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
           $v =  \Validator::make($request->all(), [
        'cat_name' => 'required',
         
    ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
      $name = $request->input('cat_name');
   
       $oCategory = new Category();
       
       $oCategory->createCategory($name);
       \Session::flash('insert_message', 'The category has been created!');
       return redirect('/categories');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
    }
   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $name = $request->input('name');
      $oCategory = new Category();
      $oCategory->updateCategory($id, $name);
       \Session::flash('update_message', 'The category has been updated!');
      return redirect('/categories');
    }

      public function subCategories()
    {
        return $this->belongsToMany('App\Role');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
       $oCategory = new Category();
       $oCategory->deleteCategory($id);
        \Session::flash('delete_message', 'The category has been deleted!');
        return redirect('/categories');
    }
     public function edit($id) {
        
         if (Session::get('logged') == 1) {
           
       $oCategory = new Category();
     $category = $oCategory->getCategoryById($id);
      return \View::make("category")->with("category", $category);
        } else {
           
        }
    }
  
}

