<html>
  
   <head>
      <title>Edit Product</title>
   </head> 
   <body>
        <h2>Edit Product</h2>
          <div class="container">
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
       <form action="<?php echo URL::to('/');?>/ProductController/<?=$product[0]->id?>" method="POST" enctype='multipart/form-data'>
          {{ method_field('PUT') }}
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <label>Name :</label>
            <input type="text" name="name" value ="<?php echo $product[0]->name; ?>" ></br>
            
            <img src="/Blog/Blog/storage/app/public/product/<?php echo $product[0]->id; ?>/<?php echo $product[0]->image_name; ?>">
            </br>
            <label>Choose image :</label>
             <input type="hidden" name="image_name" value ="<?php echo urlencode($product[0]->image_name); ?>"></br>
            <input type="file" name="file" ></br>
            </br>
            <label>Subcategory :</label>
            </br>
            <select name ="subcategory">
                <option value="Choose category">Choose subcategory</option>
                <?php foreach ($subcategories as $subcategory):?>
                <option  value ="<?php echo $subcategory->id; ?>"><?php echo $subcategory->name; ?></option>
                <?php endforeach;?>
            </select>
            </br>
         </br>
         <label>Manufacturers :</label>
            </br>
          <select name ="manufacturer">
                <option value="Choose manufacturer">Choose manufacturer</option>
                <?php foreach ($manufacturers as $manufacturer):?>
                <option  value ="<?php echo $manufacturer->id; ?>"><?php echo $manufacturer->name; ?></option>
                <?php endforeach;?>
            </select>
         </br>
         </br>
          <label>Price :</label>
            <input type="text" name="price" ></br>
         </br>
           <input type="hidden" name="_method" value="put">
            <input type="submit" value="Send" name="submit">
      </form>
   </body>
</html>