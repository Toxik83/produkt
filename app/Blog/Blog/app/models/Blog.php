<?php

namespace App\models;

use DB;

/**
 * Description of Blog
 *
 * @author USER
 */
class Blog {

    public function getBlogs() {

        $aBlogs = DB::table('blog')
                ->get();

        if (!$aBlogs->isEmpty()) {
            return $aBlogs;
        }
    }

    public function create($title, $description, $content, $image_name) {

        DB::table('blog')->insert(
                ['title' => $title, 'description' => $description, 'content' => $content, 'date_added' => date('Y-m-d'), 'image_name' => $image_name]
        );
    }

    public function delete($iBlogId) {
        DB::delete('delete from blog where id = ?', [$iBlogId]);
    }

    public function getBlogById($iBlogId) {
        $oBlog = DB::select('select * from blog where id = ?', [$iBlogId]);
        return $oBlog;
    }

    public function update($id, $title, $description, $content, $image_name) {
        DB::table('blog')
                ->where('id', $id)
                ->update(['title' => $title, 'description' => $description, 'content' => $content, 'date_added' => date('Y-m-d'), 'image_name' => $image_name]);
    }
    
     public function getPagedBlogs() {
       $blogs = DB::table('blog')->paginate(10);
       return $blogs;
    }

}
