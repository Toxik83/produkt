<!DOCTYPE html>
<html> 
    <title>Add Product</title>
    <body>
        <h2>Add Product</h2>
         <div class="container">
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
       <div class="messages">
        @if (Session::has('createProduct_message'))
        {{ Session::get('createProduct_message') }}
      @endif
       @if (Session::has('updateProduct_message'))
        {{ Session::get('updateProduct_message') }}
      @endif
        @if (Session::has('deleteProduct_message'))
        {{ Session::get('deleteProduct_message') }}
      @endif
      <form action="storeProduct" method ="POST" enctype='multipart/form-data'>
            {{ csrf_field() }}
            <label>Title :</label>
            <input type="text" name="name" ></br>
            </br>
            <label>Choose image :</label>
            <input type="file" name="file"></br>
            </br>
         <label> Subcategories :</label>
            <select name ="subcategory">
                <option value="Choose category">Choose subcategory</option>
                <?php foreach ($subcategories as $subcategory):?>
                <option  value ="<?php echo $subcategory->id; ?>"><?php echo $subcategory->name; ?></option>
                <?php endforeach;?>
            </select>
         </br>
         </br>
           <label> Manufacturers :</label>
            <select name ="manufacturer">
                <option value="Choose manufacturer">Choose manufacturer</option>
                <?php foreach ($manufacturers as $manufacturer):?>
                <option  value ="<?php echo $manufacturer->id; ?>"><?php echo $manufacturer->name; ?></option>
                <?php endforeach;?>
            </select>
         </br>
         </br>
         <label>Sizes :</label>
            <input type="text" name="sizes"></br>
         </br>
          <label>Price :</label>
            <input type="text" name="price" ></br>
         </br>
            <input type="submit" value="Save" name="submit">
            </br>
        </form>
         <h2>Products</h2>   
        <?php if (!empty($products)): ?>
            <?php foreach ($products as $iKey => $aValue) : ?>
            <tr>
                <td><?php echo htmlspecialchars($aValue->name); ?></td></br>
                <td><a href = '<?php echo URL::to('/');?>/ProductController/{{ $aValue->id }}/edit'>Edit</a></td></br>
               <td><a href = '<?php echo URL::to('/');?>/ProductController/{{ $aValue->id }}/delete'>Delete</a></td></br>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <h4>There are no records yet</h4>
<?php endif; ?> 
</body>
</html>

