<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\models\Category;
use App\models\Subcategory;
use App\models\Product;
use App\models\Manufacturer;
class SubCategoryController 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subcategories() {
        
        $oCategory = new Category();
         $oSubcategory = new Subcategory();
        $categories = $oCategory->getCategories();
        if(empty($categories)){
            return redirect('/categories');
        }
        $subcategories = $oSubcategory->getSubcategories();
    
        return view('addSubCategory', array('categories' => $categories, 'subcategories' => $subcategories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
     $oCategory = new Category();
    $categories =  $oCategory->getCategories();
    if(!empty($categories)){
       
        return view('addCategory', array('categories' => $categories));
    }
      return view('addCategory');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $name = $request->input('sub_name');
      $parent_id = $request->input('category');
        
       $oSubcategory = new Subcategory;
       $oSubcategory->create($parent_id, $name);
       return redirect('/sub');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
    }
   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
          $v =  \Validator::make($request->all(), [
        'sub_name' => 'required'
        
    ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
  $name = $request->input('sub_name');
      $oSubCategory = new Subcategory();
      $oSubCategory->updateSubCategory($id, $name);
      \Session::flash('updateSub_message', 'The subcategory has been updated!');
      return redirect('/sub');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $oSubCategory = new Subcategory();
        $oSubCategory->deleteSubCategory($id);
\Session::flash('deleteSub_message', 'The subcategory has been updated!');
        return redirect('/sub');
    }

    public function edit($id)
    {
         if (Session::get('logged') == 1) {
       $oSubCategory = new SubCategory();
       $oCategory = new Category();
       
      
   $subCategory = $oSubCategory::find($id);
    
     $categories =  $oCategory->getCategories();
      return view('subcategory', array('categories' => $categories, 'subcategory' => $subCategory));
        } else {
           
        }
    }
     public function addProduct($cat_id, $sub_id) {
        $oSubCategory = new Subcategory();
        $oProduct = new Product();
        $oManufacturer = new Manufacturer();
       $oSubCategories = $oSubCategory->getSubcategories();
       $products = $oProduct->getProductsByCatIdAndSubId($cat_id,$sub_id);
       $aManufacturers = $oManufacturer->getManufacturers();
       return view('addProduct', array('subcategories' => $oSubCategories,'products' => $products,'manufacturers'=>$aManufacturers));
    }

}

