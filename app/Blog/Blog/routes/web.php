<?php
use Illuminate\Support\Facades\Input;
use App\models\Subcategory;
use App\models\Product;
use App\models\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('login', 'LoginController@index');
Route::get('blogIndex', 'BlogController@index');
Route::get('logout', 'LoginController@logout');
Route::any('check', 'LoginController@login');

Route::get('addBlog', 'BlogController@create');
Route::post('create', 'BlogController@store');
Route::any('delete/{id}', 'BlogController@destroy');
Route::get('BlogController/{id}/edit', 'BlogController@edit');
Route::put('BlogController/{id}', 'BlogController@update');

Route::get('blogs', 'BlogController@getPagedBlogs');
Route::any('view/{id}', 'BlogController@show');

Route::get('register', 'LoginController@register');
Route::post('register', 'LoginController@saveUser');
Route::post('store', 'LoginController@checkStore');
Route::get('categories', 'CategoryController@create');
Route::get('sub', 'SubCategoryController@subcategories');
Route::post('createCategory', 'CategoryController@store');
Route::post('createSub', 'CategoryController@storeSub');
Route::get('CategoryController/{id}/edit', 'CategoryController@edit');
Route::get('SubCategoryController/{id}/edit', 'SubCategoryController@edit');
Route::put('SubCategoryController/{id}', 'SubCategoryController@update');
Route::put('CategoryController/{id}', 'CategoryController@update');
Route::any('delete/{id}', 'CategoryController@destroy');
Route::any('deleteSub/{id}', 'SubCategoryController@destroy');
Route::get('category/{cat_id}/sub/{id}', 'SubCategoryController@addProduct');
Route::post('category/{cat_id}/sub/{id}', 'ProductController@storeProduct');
Route::get('manufacturers', 'ManufacturerController@manufacturers');
Route::post('createManufacturer', 'ManufacturerController@store');
Route::put('ManufacturerController/{id}', 'ManufacturerController@update');
Route::get('ManufacturerController/{id}/edit', 'ManufacturerController@edit');
Route::get('ProductController/{id}/edit', 'ProductController@edit');
Route::put('ProductController/{id}', 'ProductController@update');
Route::get('ProductController/{id}/delete', 'ProductController@destroy');
Route::get('getProducts', 'ProductController@getProducts');
//Route::post('getSubCategories', 'ProductController@getSubCategories');

Route::post('postCategoryId', 'ProductController@postCategoryId');
Route::get('/subcat', function () {
    $id = Input::get('cat-id');
     $oSubCategory = new Subcategory();
     $subcategories  = $oSubCategory->getSubCategoriesByParentId($id);
    return Response::json(array('subcategories'=>$subcategories,'id'=>$id));
});
Route::get('products', 'ProductController@getProducts');
Route::get('getProducts', ['uses' => 'ProductController@getProducts', 'as'=>'products']);
Route::get('{id?}/{sub_id?}', 'ProductController@getProducts');
Route::post('{id?}/{sub_id?}', 'ProductController@getProducts');
Route::get('view/{id}', 'ProductController@show');
Route::post('cart', 'CartController@cart');
Route::get('cart', 'CartController@cart');
Route::get('cart/checkout', 'CartController@checkout');
Route::post('cart/order', 'CartController@order');
Route::post('checkout', 'CartController@checkout');
Route::post('cartUpdate', ['uses' => 'CartController@cartUpdate', 'as'=>'cartUpdate']);
Route::get('deleteFromCart/{rowId}', 'CartController@delete');
Route::get('orders', 'OrderController@showOrders');
Route::get('details/{id}', 'OrderController@details');
Route::get('paid/{id}', 'OrderController@paid');
Route::get('confirm/{id}', 'OrderController@confirm');
Route::get('cancel/{id}', 'OrderController@cancel');

