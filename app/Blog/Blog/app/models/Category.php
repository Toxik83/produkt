<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Description of Category
 *
 * @author USER
 */
class Category  extends Model {
    
    protected $table = 'category';
    //put your code here
     public $timestamps = false;
      public function createCategory($name) {
         
        DB::table('category')->insert(
                ['name' => $name]
        );
    }
    public function getCategories() {

        $aCategories = DB::table('category')
                ->get();

        if (!$aCategories->isEmpty()) {
            return $aCategories;
        }
    }
    
    public function getCategoryById($id) {
        $oCategory = DB::select('select * from category where id = ?', [$id]);
        return $oCategory;
    }
    
     public function updateCategory($id, $name) {
        DB::table('category')
                ->where('id', $id)
                ->update(['name' => $name]);
    }
    
      public function deleteCategory($id) {
        DB::delete('delete from category where id = ?', [$id]);
    }
     public function subCategories()
    {
        return $this->hasMany('App\models\Subcategory', 'parent_id');
    }
    public function getSubcategoriesById($id) {
        $aSubs= DB::select('select category.id as cid, subcategories.id as sid, category.*, subcategories.* from category JOIN subcategories ON category.id = subcategories.parent_id where id = ?', [$id]);
        var_dump($aSubs); die;
        return $aSubs;
    }
}
