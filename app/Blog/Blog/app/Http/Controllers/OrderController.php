<?php

namespace App\Http\Controllers;

use App\models\Order;
use Illuminate\Http\Request;
use Session;

class OrderController 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
   $oOrder = new Order();
     $details = $oOrder->orderDetails($id);
     return view('details', array('details' => $details));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
       
    }
    public function paid($id) {
         $oOrder = new Order();
         $oOrder->paid($id);
         return back();
    }

        public function confirm($id) {
         $oOrder = new Order();
         $oOrder->confirm($id);
         return back();
    }
    
       public function cancel($id) {
         $oOrder = new Order();
         $oOrder->cancel($id);
         return back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showOrders()
    {
     $oOrder = new Order();
     $orders = $oOrder->allOrders();
     
     return view('orders', array('orders' => $orders));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
       
    }
 public function getPagedBlogs() {
     
    }
}
