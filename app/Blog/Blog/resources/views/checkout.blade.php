<!DOCTYPE html>

<html lang="en">
  <head>
   <form action="order" method ="POST">
       <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_wQiTJOuxWmRsHK7VcvGUqlK0"
    data-amount="{{ $total }}"
    data-name="Demo Site"
    data-description="Widget"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto">
  </script>
</form>
  </body>
</html>
