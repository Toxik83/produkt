<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\User;
use App\models\Blog;
use Session;

class LoginController {
    
    public function index() {
        echo view('login');
    }
    public function register() {
        echo view('register');
    }
    
    public function saveUser(Request $request) {
         $v =  \Validator::make($request->all(), [
        'username' => 'required',
           'email' => 'required',  
        'password' => 'required'
            ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $hash = md5($password);
       $oUser = new User();
       $oUser->createUser($username, $email, $hash);
    }
    public function logout() {
        Session::forget('logged');
        echo view('login');
    }
    public function login(Request $request) {
        $oUser = new User();
        if (isset($_POST)) {
         
            $username = $request->input('username');
            $password = $request->input('password');
            $trimmedUsername = trim($username);
            $trimmedPassword = trim($password);
            
            $hash = md5($trimmedPassword);
         
            $oUser = $oUser->checkUser($username, $hash);
      
            if ($oUser !== null) {
                Session::put('logged', 1);
            }
            if (Session::get('logged') == 1) {  
                $oBlog = new Blog();
                $aBlogs = $oBlog->getBlogs();
                return view('dashboard', array('aBlogs' => $aBlogs));
            } else {
                return view('login');
            }
        }
    }
       public function checkStore(Request $request) {
        $oUser = new User();
        if (isset($_POST)) {
     $v =  \Validator::make($request->all(), [
        'username' => 'required',
                        'password' => 'required'
            ]);
     
     if ($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
            $username = $request->input('username');
            $trimmedUsername = trim($username);
            $password = $request->input('password');
            $trimmedPassword = trim($password);
            
            $hash = md5($trimmedPassword);
            
            $oUser = $oUser->checkUser($username, $hash);
                                
            if ($oUser !== null && $oUser[0]->role == 'user') {
                Session::put('logged', 2);
                Session::put('member_id', $oUser[0]->id);
                 return redirect('/products');
            }
            else if($oUser !== null && $oUser[0]->role == 'admin'){
              Session::put('logged', 1);
                return view('dashboard');
            }
             else {
                return view('index');
            }
        }
    }
}
