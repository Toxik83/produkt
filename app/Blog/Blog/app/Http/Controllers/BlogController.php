<?php

namespace App\Http\Controllers;

use App\models\Blog;
use Illuminate\Http\Request;
use Session;

class BlogController 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if (Session::get('logged') == 1) {
            echo view('addBlog');
        } else {
            return view('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $title = $request->input('title');
        $description = $request->input('description');
        $content = $request->input('editor');

        $file_ext = explode('.', $_FILES['file']['name']);
        $ext = $file_ext[1];
        $allowed = array('jpg');

        $file_name = $_FILES['file']['name'];
        $file_destination = 'C:/xampp/htdocs/Blog/Blog/storage/app/public/' . basename($_FILES['file']['name']);

        if (in_array($ext, $allowed)) {

            if (move_uploaded_file($_FILES['file']['tmp_name'], $file_destination)) {
                
            } else {
                echo "Upload failed";
            }
            $oBlog = new Blog();
            $oBlog->create($title, $description, $content, $file_name);
            echo "Record added successfully.<br/>";
            return redirect('/blogIndex');
        } else {
            echo "Choose  picture frm another type<br/>";
            return redirect('/blogIndex');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $oBlog = new Blog();
      $blog = $oBlog->getBlogById($id);
      return \View::make("view")->with("blog", $blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         if (Session::get('logged') == 1) {
             $oBlog = new Blog();
      $blog = $oBlog->getBlogById($id);
    return \View::make("blog")->with("blog", $blog);
        } else {
            return view('login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $title = $request->input('title');
        $description = $request->input('description');
        $content = $request->input('editor');

        $file_ext = explode('.', $_FILES['file']['name']);
        $ext = $file_ext[1];
        $allowed = array('jpg');

        unlink('C:/xampp/htdocs/Blog/Blog/storage/app/public/' . $request->input('image_name'));

        $new_image = $_FILES['file']['name'];

        if (in_array($ext, $allowed)) {
            if (move_uploaded_file($_FILES['file']['tmp_name'], 'C:/xampp/htdocs/Blog/Blog/storage/app/public/' . $new_image)) {
                //  echo "File is valid, and was successfully uploaded.\n";
            } else {
                echo "Upload failed";
            }
            $oBlog = new Blog();
            $oBlog->update($id, $title, $description, $content, $new_image);
            return redirect('/blogIndex');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $oBlog = new Blog();
        $oBlog->delete($id);
        echo "Record deleted successfully.<br/>";
        return redirect('/blogIndex');
    }
 public function getPagedBlogs() {
      $oBlog = new Blog();
       $blogs = $oBlog->getPagedBlogs();
       return view('blogs', array('blogs' => $blogs));
    }
}
