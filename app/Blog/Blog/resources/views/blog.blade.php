<html>
   
   <head>
      <title>Student Management | Edit</title>
      <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
   </head>
  
   <body>
       <form action="<?php echo URL::to('/');?>/BlogController/<?=$blog[0]->id?>" method="POST" enctype='multipart/form-data'>
          {{ method_field('PUT') }}
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <label>Title :</label>
            <input type="text" name="title" value ="<?php echo $blog[0]->title; ?>" ></br>
            <label>Description :</label>
            <input type="text" name="description" value ="<?php echo $blog[0]->description; ?>"></br>
            <label>Content :</label>
            <textarea class ='ckeditor'name ='editor'><?php echo strip_tags($blog[0]->content); ?></textarea>
           <img src="/Blog/Blog/storage/app/public/<?php echo $blog[0]->image_name; ?>">
           <input type="hidden" name="_method" value="put">
            <label>Choose image :</label>
             <input type="hidden" name="image_name" value ="<?php echo urlencode($blog[0]->image_name); ?>"></br>
            <input type="file" name="file" ></br>
            <input type="submit" value="Send" name="submit">
      </form>
   </body>
</html>