<!DOCTYPE html>
<html> 
    <title>Add manufacturer</title>
    <body>
       
        <h2>Add manufacturer</h2>
         <div class="container">
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
      <div class="messages">
        @if (Session::has('insertManufacturer_message'))
        {{ Session::get('insertManufacturer_message') }}
      @endif
       @if (Session::has('update_message'))
        {{ Session::get('update_message') }}
      @endif
        @if (Session::has('delete_message'))
        {{ Session::get('delete_message') }}
      @endif
      </div>
        <form action="createManufacturer" method ="POST">
            {{ csrf_field() }}
            <label>Name :</label>
            <input type="text" name="name" ></br>
            <label>OrderId:</label>
            <input type="text" name="order_id" ></br>
            <input type="submit" value="Save" name="submit">
            </br>
        </form>
     

        <h2>Manufacturers</h2>   
        <?php if (!empty($manufacturers)): ?>
            <?php foreach ($manufacturers as $iKey => $aValue) : ?>
            <tr>
                <td><?php echo htmlspecialchars($aValue->name); ?></td></br>
                <td><a href = 'ManufacturerController/{{ $aValue->id }}/edit '>Edit</a></td></br>
                <td><a href = 'delete/{{ $aValue->id }}'>Delete</a></td></br>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <h4>There are no records yet</h4>
<?php endif; ?> 
</body>
</html>

