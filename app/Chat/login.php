<?php
session_start();
?>
<head>
    <title>Chat</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
</head>
<div id="wrapper">
    <div id="menu">
        <p class="welcome">Welcome, <b></b></p>
        <p class="logout"><a id="exit" href="#">Exit Chat</a></p>
        <div style="clear:both"></div>
    </div>
    <div id="chatbox">
    </div>
    <div id="chatMessages">

    </div>
    <form action="chat.php" method="post">
        Name: <input  id = "name" type="text" name="name"><br>
        Message: <input id = "message" type="text" name="email"><br>
        <input id="send" type="button" value="Submit">
    </form>
</div>
</body>
</html>
<script>
    $(document).ready(function () {

        $("#send").click(function () {
            var name = $('#name').val();
            var message = $('#message').val();

            $.ajax({
                url: "InsertMessage.php",
                type: "POST",
                dataType: 'json',
                data: {name: name, message: message},
            });
        });
        ajax_call = function () {
            $.ajax({//create an ajax request to load_page.php
                type: "GET",
                url: "DisplayMessages.php",
                dataType: "json", //expect json to be returned                
                success: function (data) {
                    jQuery.each(data, function (index, item) {
                        $('#chatMessages').append("<span class='label label-important'>" + item.name + " says:" + '</span></br>');
                        $('#chatMessages').append("<span class='label label-important'>" + item.message + '</span></br>');
                    });
                }
            });
        };
        var interval = 2000;
        setInterval(ajax_call, interval);
    });
</script>