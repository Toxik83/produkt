<?php
namespace App\models;
use DB;
/**
 * Description of Category
 *
 * @author USER
 */
class Product {
   public function create($name, $cat_id,$price,$manufacturer,$sub_id,$file_name, $sizes) {
         
    $id =    DB::table('product')->insertGetId(
                ['name' => $name,'price' => $price,'manufacturer' => $manufacturer,'cat_id' => $cat_id,'sub_id' => $sub_id, 'image_name'=>$file_name,'sizes' => $sizes]
        );
        return $id;
    }
     public function update($id, $name,$cat_id,$price,$manufacturer, $sub_id,$new_image) {
         
        DB::table('product')
                ->where('id', $id)
                ->update(['name' => $name, 'price' => $price,'manufacturer' => $manufacturer,'cat_id' => $cat_id,'sub_id' => $sub_id, 'image_name' => $new_image]);
    }
    
     public function getProductsByCatIdAndSubId($cat_id,$sub_id) {

        $products = DB::table('product')
                ->where('cat_id', $cat_id)
                ->where('sub_id', $sub_id)
                ->get();
        
        if (!$products->isEmpty()) {
            return $products;
        }
    }
     public function getProductById($id) {
        $product = DB::select('select * from product where id = ?', [$id]);
        return $product;
    }
    
      public function getPagedProducts($sub_id = 0, $man_id = 0) {
          
          if(!$sub_id ==0){
             
              $pagedProducts = DB::table('product')
                ->where('sub_id', $sub_id)->paginate(1); 
          }
           else if(!$man_id ==0){
            
               $pagedProducts = DB::table('product')
                ->where('manufacturer', $man_id)->paginate(1); 
        }
       
           else if(!$sub_id ==0 || $man_id == 0){
             
                      $pagedProducts = DB::table('product')->paginate(1);
            
        }
        return $pagedProducts;
 
    }
     public function deleteProduct($id) {
        DB::delete('delete from product where id = ?', [$id]);
    }
}
