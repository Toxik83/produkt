<?php 
namespace Chat;

class User
{
private $id;
private $name;
private $message;
private $timeposted;

public function setName($userName){
	$this->name = $userName;
}
	public function getName(){
	return $this->name;
}

public function setMessage($message){
	$this->message = $message;
}
	public function getMessage(){
	return $this->message;
}

public function setTimePosted($time){
	$this->timeposted = $time;
}
	public function getTimePosted(){
	return $this->timeposted;
}

public function addUser(){
	include 'conn.php';
	$stmt = $db->prepare('INSERT INTO chat (name, message, time) VALUES (:name, :message, NOW())');
	$stmt->bindParam(':name',$this->name);
    $stmt->bindParam(':message', $this->message);
	$stmt->execute();
}

public function displayMessages(){
	include 'conn.php';
	$stmt = $db->prepare('SELECT * FROM chat WHERE time BETWEEN NOW() - INTERVAL 2 SECOND AND NOW()  ORDER BY time ASC');
	$stmt->execute();
	$aChat = array();
	while($r = $stmt->fetch()){
 $aChat[] = array(
        'name' => $r['name'],
        'message' => $r['message'],
		'time'=> $r['time']	
    );
	 }
return $aChat;
}	
}

