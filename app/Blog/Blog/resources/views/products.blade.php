<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content=""> 

        <link rel="icon" href="../../favicon.ico">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style>
 

          
        </style>

        <title>Navbar Template for Bootstrap</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="{{ asset('css/signin.css') }}" rel="stylesheet" type="text/css" >
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
          <div class="messages">
        @if (Session::has('order'))
        {{ Session::get('order') }}
      @endif
        <div class="container">
             <div id="nav">
          @foreach($categories as $category_menu)
         <ul>
              <li>
                  <a href="{{ $category_menu['id'] }}">{{ $category_menu['name'] }} </a>
               </li>  
        </ul>
@endforeach
 <div id="manufacturers">
       <ul>
                            @foreach($manufacturers as $manufacturer)
                              <li class=""><a href="{{ route('products', array('man_id' => $manufacturer->id)) }}">{{$manufacturer->name}}</a></li>
                            @endforeach
                          </ul>    

 </div>
               <?php if (!empty($subcategories)): ?>
           <div id="subs">
               <form action="products" method ="POST">
                   {{ csrf_field() }}
          @foreach($subcategories as $category_menu)
         <label>
            <input type="checkbox" name="sub[]" value=" {{ $category_menu->id }}"> {{ $category_menu->name }}
            </label>
@endforeach
 <input type="submit" value="Save" name="submit">
        </form>
<?php else: ?>
<?php endif; ?> 
   
             </div>
            <table width="60%" cellspacing="0" cellpadding="4" border="0" class="data">
                 <?php if (!empty($products)): ?>
    @foreach($products as $product)
    <tr>
        <td> {{ $product->name }} </td>
    <a href="view/<?php echo $product->id; ?>"> <img src="/Blog/Blog/storage/app/public/product/<?php echo $product->id; ?>/<?php echo $product->image_name; ?>"></a>
    </tr>
    @endforeach
  </table>
            <div class="pagination"> {{ $products->appends(request()->all())->render() }} </div>
            <?php else: ?>
<?php endif; ?> 
        </div> <!-- /container -->
    </body>

    <script>
    </script>
</html>

