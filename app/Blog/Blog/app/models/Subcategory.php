<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Description of Category
 *
 * @author USER
 */
class Subcategory extends Model {
    //put your code here
     protected $table = 'subcategories';
     
      protected $primaryKey = 'id';
     
     protected $fillable = ['name'];
      public function create($parent_id,$name) {
         
    $id =    DB::table('subcategories')->insertGetId(
                ['parent_id' =>$parent_id,'name' => $name]
        );
         DB::table('category_subs')->insert(
                ['cat_id' => $parent_id,'sub_id' => $id]
        );
    }
    public function getSubcategories() {

        $aSubcategories = DB::table('subcategories')
                ->get();

        if (!$aSubcategories->isEmpty()) {
            return $aSubcategories;
        }
    }
    
    public function getSubCategoryById($id) {
        $oSubCategory = DB::select('select subcategories.id, subcategories.parent_id, subcategories.name as sub,category.name from subcategories JOIN category  ON category.id = subcategories.parent_id where subcategories.id = ?', [$id]);
        return $oSubCategory;
    }
    
     public function updateSubCategory($id, $name) {
        DB::table('subcategories')
                ->where('id', $id)
                ->update(['name' => $name]);
    }
    
      public function deleteSubCategory($id) {
        DB::delete('delete from subcategories where id = ?', [$id]);
    }
      public function getSubCategoriesByParentId($cat_id) {
        $subcategories = DB::select('select category_subs.id as csubs_id,subcategories.* FROM category_subs JOIN subcategories ON category_subs.sub_id = subcategories.id WHERE category_subs.cat_id =?', [$cat_id]);
        return $subcategories;
    }
}
