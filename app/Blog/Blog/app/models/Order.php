<?php
namespace App\models;
use DB;
/**
 * Description of Category
 *
 * @author USER
 */
class Order {
    //put your code here
    
     public function orderItems($name, $quantity,$price,$order_id) {
         
        DB::table('order_items')->insert(
                ['name' => $name,'quantity' => $quantity,'price' => $price,'order_id' => $order_id]
        );
    }
      public function order($user_id, $address,$amount,$payment_id) {
         
      $id =  DB::table('orders')->insertGetId(
                ['user_id' => $user_id,'address' => $address,'amount' => $amount, 'payment_id' => $payment_id]
        );
      return $id;
    }
    public function allOrders() {
     $orders= DB::table('orders')
             ->join('user', 'orders.user_id', '=', 'user.id')
             ->select('orders.id as order_Id','orders.*', 'user.id as user_Id', 'user.*')
             ->orderBy('orders.id')
             ->get(); 
     
      return $orders;
    }
     public function orderDetails($id) {
$details = DB::select('select * from orders JOIN order_items ON orders.id = order_items.order_id WHERE orders.id = ?', [$id]);
    
      return $details;
    }
    //status  za paid  e 3
       public function paid($id) {
    
     DB::table('orders')
            ->where('id', $id)
            ->update(['status' => 3]);
    }
    //status za  confirm e 1
        public function confirm($id) {
    
     DB::table('orders')
            ->where('id', $id)
            ->update(['status' => 1]);
    }
    //status
     public function cancel($id) {
    
     DB::table('orders')
            ->where('id', $id)
            ->update(['status' => 2]);
    }
}
