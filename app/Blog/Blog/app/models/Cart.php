<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Description of Category
 *
 * @author USER
 */
class Cart  extends Model {
    
    protected $table = 'cart';
    //put your code here
     public $timestamps = false;
     
   public function products($member_id){
       $cart =   DB::table('cart')
            ->join('product', 'cart.product_id', '=', 'product.id')
            ->select('*')
            ->get();
      return $cart;
   }
           
       public function create($member_id,$product_id,$amount,$total) {
         
        DB::table('cart')->insert(
                ['member_id' => $member_id,'product_id' => $product_id, 'amount' =>$amount, 'total' =>$total]
        );
    }
}
