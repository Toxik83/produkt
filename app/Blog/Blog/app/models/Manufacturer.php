<?php

namespace App\models;

use DB;

/**
 * Description of Blog
 *
 * @author USER
 */
class Manufacturer {

    public function getManufacturers() {

        $aManufacturer = DB::table('manufacturer')
                ->get();

        if (!$aManufacturer->isEmpty()) {
            return $aManufacturer;
        }
    }

    public function create($name, $order_id) {

        DB::table('manufacturer')->insert(
                ['name' => $name, 'order_id' => $order_id]
        );
    }

    public function delete($iBlogId) {
        DB::delete('delete from blog where id = ?', [$iBlogId]);
    }

    public function getBlogById($iBlogId) {
        $oBlog = DB::select('select * from blog where id = ?', [$iBlogId]);
        return $oBlog;
    }

    public function update($id, $name) {
        DB::table('manufacturer')
                ->where('id', $id)
                ->update(['name' => $name]);
    }
    
     public function getPagedBlogs() {
       $blogs = DB::table('blog')->paginate(10);
       return $blogs;
    }
  public function getManufacturerById($id) {
        $manufacturer = DB::select('select * from  manufacturer where id = ?', [$id]);
        return $manufacturer;
    }
}
