<?php

namespace App\Http\Controllers;

use App\models\User;
use App\models\Order;
use Illuminate\Http\Request;
use Session;
use App\models\Product;
use Gloudemans\Shoppingcart\Facades\Cart  as Cart;
class CartController 
{
  public function cart(Request $request)
{
    if ($request->isMethod('post')) {
        $product_id = $request->input('product_id');
        $oProduct = new Product();
        $product = $oProduct->getProductById($product_id);   
        Cart::add(array('id' => $product_id, 'name' => $product[0]->name, 'qty' => 1, 'price' => $product[0]->price));
        
          return redirect('cart')->withSuccessMessage('Item was added to your cart!');
    }
    return view('cart');
}
public function cartUpdate(Request $request)
{
Cart::update($request->input('rowid'),$request->qty);
return back();
}
public function delete(Request $request,$rowId)
{
Cart::remove($rowId);
return back();
}
public function checkout(Request $request)
{
    $total = Cart::subtotal() * 100;
 return view('checkout', ['total' => $total]);
}
public function order(Request $request)
{
   
    //var_dump(Cart::content()); 
\Stripe\Stripe::setApiKey(env('stripe_key'));
$token = $_POST['stripeToken'];
 $charge = \Stripe\Charge::create(array(
                        'amount' => (Cart::total()*100),
                        'currency' => 'usd',
                        'description' => "test",
                        'card' => $token
                    ));
 //order send here; Edin user ima mnogo pora4ki, a edna pora4ka e kam edin user
 $oOrder = new Order();
$total = Cart::subtotal();
$order_id = $oOrder->order($request->session()->get('member_id'), '', $total, $charge->id);

 foreach(Cart::content() as $row){
    $oOrder = new Order();
    $oOrder->orderItems($row->name, $row->qty, $row->price, $order_id);
 }
                  $request->session()->forget('cart');
                     return redirect('products')->with('order', 'The order is send!');
}
}